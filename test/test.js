const assert = require('assert');
const evenFibNums = require('../index.js');

describe('Problem2', function() {
  it('sum of even Fibonacci numbers where the values do not exceed 4,000,000 is correct!', function() {
    assert.equal(evenFibNums(), 4613730);
  });
});
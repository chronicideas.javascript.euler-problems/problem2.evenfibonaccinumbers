function evenFibNums() {
  let a = 1;
  let b = 2;
  let c = a + b;
  let sum = 0;

  while(sum < 4000000) {
    a = b;
    b = c;
    c = a+b;

    if(c%2 == 0) {
      sum = sum + c;
    }
  }
  return sum;
}

module.exports = evenFibNums;